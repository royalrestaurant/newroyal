<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css"
          href="https://fonts.googleapis.com/css?family=Font+Name">
    <style>
      body {
        font-family: 'FontName', serif;
        font-size: 30px;
      }
      .button {
    background-color: #ff9900; 
    border: none;
    color: white;
    padding: 16px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block; 
    font-size: 16px;
    margin: 10px 10px;
    -webkit-transition-duration: 0.4s; 
    transition-duration: 0.4s;
    cursor: pointer;
    
   
} 
    </style>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>View All Items</title>
</head>

<body>

	<!--@author naveen 
	creating a table to see all items, admin can modify or delete items;
this jsp file get list of items from ViewAllItemsServlet -->

	<h2 align="center">All Items in Royal Restaurant</h2>
<div style="float: left;">
<input type = "button" onclick="history.back()" value="Back" class="button"/></div>
	<table border="1px solid black" align="center" style="text-align:center">
		<tr>
			<th>Item No</th>
			<th>Item Name</th>
			<th>Category</th>
			<th>Availability</th>
			<th>Price</th>
			<th>Cooking Time</th>
			<th>Edit</th>
			<th>Delete</th>
		</tr>
		<c:forEach var="items" items="${allitems }">
			<tr>
				<td>${items.itemNo }</td>
				<td>${items.itemName }</td>
				<td>${items.category }</td>
				<td>${items.availability }</td>
				<td>${items.price }</td>
				<td>${items.cookingTime }</td>
				<td><a href="EditServlet?itemNo=${items.itemNo }">Edit</a></td>
				<td><a href="deleteItemServlet?itemNo=${items.itemNo }">Delete</a></td>
			</tr>
		</c:forEach>
	</table>
	<br>
	


</body>
</html>