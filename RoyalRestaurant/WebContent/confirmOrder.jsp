<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css"
          href="https://fonts.googleapis.com/css?family=Font+Name">
          <style>
          body {
        font-family: 'FontName', serif;
        font-size: 30px;
        color:white;
      }
      </style>
<title>ConfirmOrder</title>
<style>
table{
border-collapse: collapse;
}
table, th,td {
    border: 1px solid #a9303b;
}
</style>
</head>
<div style="position:absolute;left:30%;top:10%">
<body background="images/wall2.jpg">
<h3 style="color:white">Your Order</h3>
<form action="removeFromCart" method="post">
<table style="color: white">

<c:set var="count" value="${0}"></c:set>
<tr>
<th>CheckItems</th>
<th>Name</th>
<th>Category</th>
<th>Quantity</th>
<th>Price</th>
<th>CookingTime</th>
</tr>
<c:forEach var="item" items="${ItemList}" >
<tr>
<c:set var="count" value="${count+1}"></c:set>     
<td><input type="checkbox" value="${count}" name="removeitems"></td>
<td>${item.itemName}</td>
<td>${item.category}</td>
<td>${item.quantity}</td>
<td>${item.price*item.quantity}</td>
<td>${item.cookingTime*item.quantity}</td>
</tr>
</c:forEach>
</table><br><br>
<input type="submit" value="removefromcart"/>
<input type="button" value="addMoretoCart" onclick="location.href='customer.jsp';"/>
<input type="button" value="confirmorder" onclick="location.href='displayOrderServlet';"/>
</form>
</div>
</body>
</html>