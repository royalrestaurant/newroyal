<%--by Parantap --%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>ADD Item Form</title>
<style>
.button {
    background-color: #ff9900; 
    border: none;
    color: white;
    padding: 16px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block; 
    font-size: 16px;
    margin: 10px 10px; 
    -webkit-transition-duration: 0.4s; 
    transition-duration: 0.4s;
    cursor: pointer;
    
   
} 
</style>
</head>
<body>
<div style="float: left;">
<input type = "button" onclick="history.back()" value="Back" class="button"/></div>
<a href="index.html" style="float: right"><button class="button">Log Out</button></a> 

<jsp:include page="additemform.html"></jsp:include>
</body>
</html>