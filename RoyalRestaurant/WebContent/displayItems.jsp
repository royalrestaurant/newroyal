<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<form action="getOrderItems" method="post">
<table id="ItemsTable" style="color: white">
<tr>
<th>check items</th>
<th>ItemName</th>
<th>Price</th>
<th>CookingTime</th>
<th>quantity</th>
</tr>
<c:forEach var="item" items="${ItemList}">
<tr>
<td> <input type="checkbox" name="items" value="${item}"/></td>
<td>${item.itemName}</td>
<td>${item.price}</td>
<td>${item.cookingTime}</td>
<td><input name="${item.itemNo}" type="number" min="1" max="5" value=1></td>
</tr>
</c:forEach>
</table><br><br>
<input type="submit" value="ADDTOCART">
</form>

</body>
</html>