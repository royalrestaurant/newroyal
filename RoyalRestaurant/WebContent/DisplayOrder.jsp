<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Display Order</title>
<link rel="stylesheet" type="text/css"
          href="https://fonts.googleapis.com/css?family=Font+Name">
<style>
table{
border-collapse: collapse;
}
table, th,td {
    border: 1px solid #a9303b;
}
body{
color:white
font-family: 'FontName', serif;
        font-size: 30px;
        color:white;
}
</style>
</head>
<body background="images/wall2.jpg">
<div style="position:absolute;left:30%;top:10%">
<b>Your order</b>
<table style="color: white">
<c:forEach var="item" items="${ORDER.orderedItems}" >
<tr>
<td>${item.itemName}</td>
<td>${item.category}</td>
<td>${item.quantity}</td>
<td>${item.price*item.quantity}</td>
<td>${item.cookingTime*item.quantity}</td>
</tr>
</c:forEach>
<tr>
<td colspan="2"><b>TotalBill=Rs${ORDER.totalbill}</b></td>
<td colspan="3"><b>TotalCookingTime=${ORDER.totalCookingTime()}Sec</b></td>
<tr>
</table><br>
<form action="placeOrder">
<b>choose payment mode</b>
<select name="paymentmode">
<option value="cash">Cash</option>
<option value="card">Card</option>
</select><br><br>
<input type="submit" value="placeorder"/>
</form>
</div>
</body>
</html>