<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet" type="text/css"
          href="https://fonts.googleapis.com/css?family=Font+Name">
      
<style>
table{
border-collapse: collapse;
}
table, th,td {
    border: 1px solid #a9303b;
}
body {
        font-family: 'FontName', serif;
        font-size: 30px;
        color:white
      }
</style>
<title>Customer</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>
	$(document).ready(function() {

		$("#choose").change(function() {
			$.ajax({
				url : "itemsCategory",
				data : ($("select").serialize()),
				success : function(data) {
					$("#display").html(data);
				}
			});
		});
		
			
		
	});
	
</script>
			

</head>
<body background="images/wall2.jpg" >
<div id="center" style="position:absolute;left:30%">
<b style="color:white">Choose Items</b>
<select id="choose" onchange="Display();changeimage();" name="category">
<option value="">Select</option>
<option value="Veg">VEG</option>
<option value="NonVeg">NONVEG</option>
<option value="Beverage">BEVERAGES</option>
</select> <input type="button" value="FinishOrder" onclick="location.href='finishOrderServlet';"><br><br>
<div id="display">
</div>
</div>
<!-- <div id="vegimage" style="float:right">
<img src="images/Vegimage.jpg">
</div> -->
	
</body>
</html>