<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>EDIT ITEMS</title>
<link rel="stylesheet" type="text/css"
          href="https://fonts.googleapis.com/css?family=Font+Name">
    <style>
      body {
        font-family: 'FontName', serif;
        font-size: 30px;
      }
      .button {
    background-color: #ff9900; 
    border: none;
    color: white;
    padding: 16px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block; 
    font-size: 16px;
    margin: 10px 10px;
    -webkit-transition-duration: 0.4s; 
    transition-duration: 0.4s;
    cursor: pointer;
    
   
} 
    </style>
</head>
<body>
<div style="float: left;">
<input type = "button" onclick="history.back()" value="Back" class="button"/></div>

<a href="index.html" style="float: right"><button class="button">Log Out</button></a> 
<h1 style="text-align:center">EditItem</h1>
<form action = "EditServletDisplay">

<table align="center">
<tr><td>ItemName:<td><input type="text" style = "font-size:20px" name="itemname" value = "${id.getItemName()} " readonly="readonly"/></td></tr>
<tr><td>ItemNo:</td><td><input type="text" style = "font-size:20px" name="itemno" value = "${id.getItemNo()}" readonly="readonly"/></td></tr>
<tr><td>Price:</td><td><input type="text" style = "font-size:20px" name="price" value = "${id.getPrice()}"/></td></tr>
<tr><td>CookingTime:</td><td><input type="text" style = "font-size:20px" name="cookingtime" value = "${id.getCookingTime()}"/></td></tr>
<tr><td>Category:</td><td><select name = "category" style = "font-size:20px"><option value = "Veg">Veg</option><option value = "Non Veg">Non Veg</option><option value = "Beverage">Beverage</option></select> </td></tr>
<tr><td>Availability:</td><td><select name = "availability" style = "font-size:20px"><option value = "Y">Yes</option><option value = "N">No</option></select></td></tr>
<tr><td colspan="2"><input type="submit" value="Edit Item" style="font-size:20px"/></td></tr>  
</table>
</form>      
</body>
</html>

