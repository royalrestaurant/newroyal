package com.hotel.service;

import java.util.List;

import com.hotel.bean.Item;
import com.hotel.bean.Order;

public interface IOrder {
	public List<Item> getItemsByCategory(String category);
	public boolean addOrder(Order order);
}
