package com.hotel.service;

import java.util.List;

import com.hotel.bean.Item;
//@author rohan
public interface IItem {
	public boolean addItem(Item item);
	public boolean updateItem(Item item);
	public boolean deleteItem(int itemNo);
	public List<Item> getAllItems();
	public List<Item> getItemsByCategory(String category);
	public Item getitemsByItemNo(int itemNo);
	
}
