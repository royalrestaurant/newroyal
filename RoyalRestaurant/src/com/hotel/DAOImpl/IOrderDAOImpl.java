package com.hotel.DAOImpl;

import java.util.ArrayList;
import java.util.List;
import java.sql.*;

import com.hotel.DAOService.DBConnect;
import com.hotel.DAOService.IOrderDAO;
import com.hotel.bean.Item;
import com.hotel.bean.Order;
import com.hotel.bean.OrderedItem;

import java.text.*;
public class IOrderDAOImpl implements IOrderDAO {
	static Connection conn;
	DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
	@Override
	public List<Item> getItemsByCategoryfromDB(String category) {
		conn = DBConnect.openConnection();
		String sql="select * from items where item_availability=? and item_category=?";
		PreparedStatement statement=null;
		try {
			statement=conn.prepareStatement(sql);
			statement.setString(1,"Y");
			statement.setString(2,category);
			ResultSet rs=statement.executeQuery();
			List<Item> itemList=new ArrayList<Item>();
			while(rs.next()){
				Item item=new Item();
				item.setItemNo(rs.getInt(1));
				item.setItemName(rs.getString(2));
				item.setPrice(rs.getInt(3));
				item.setCookingTime(rs.getInt(4));
				item.setCategory(rs.getString(5));
				itemList.add(item);
			}
			
			return itemList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		
		return null;
	}
	
	public void getSequenceNum(Order order){
		conn = DBConnect.openConnection();
		String sqlIdentifier = "select ORDERS_SEQ.NEXTVAL from dual";
		PreparedStatement pst;
		try {
			pst = conn.prepareStatement(sqlIdentifier);
			 ResultSet rs = pst.executeQuery();
			   if(rs.next())
			     order.setOrderno(rs.getInt(1)+1);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	  System.out.println("OrderNo"+order.getOrderno());
	}

	@Override
	public boolean addOrdertoDB(Order order) {
		conn = DBConnect.openConnection();
		String insertQuery="insert into orders values(?,?,?,?)";
		PreparedStatement statement=null;
		try {
			statement = conn.prepareStatement(insertQuery);
			statement.setInt(1,order.getOrderno());
			statement.setDate(2, new java.sql.Date(System.currentTimeMillis()));
			statement.setString(3,order.getPaymentMode());
			statement.setInt(4, order.getTotalbill());
			statement.executeQuery();
			addFoodOrdertoDB(order);
			System.out.println("added order");
            return true;      
		} catch (SQLException e) {
			
		}finally {
			try {
				if (statement != null){
					statement.close();
				DBConnect.closeConnection();
				}
				
			} catch (SQLException e) {
				System.out.println(e.getMessage());
				
			}
		}
		return false;
	}
  public boolean addFoodOrdertoDB(Order order){
	  String insertQuery="insert into orderinfo values(?,?,?)";
		PreparedStatement statement=null;
	
		ArrayList<OrderedItem> items=order.getOrderedItems();
		System.out.println(items.size());
		
		for(int i=0;i<items.size();i++){
			conn =DBConnect.openConnection();
		
		try {
			statement = conn.prepareStatement(insertQuery);
			statement.setInt(1,order.getOrderno());
			statement.setInt(2,items.get(i).getItemNo());
			statement.setInt(3,items.get(i).getQuantity());
			
			statement.executeQuery();
			System.out.println("added order");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}finally {
			try {
				if (statement != null){
					statement.close();
				DBConnect.closeConnection();
				
				}
				
			} catch (SQLException e) {
				
				System.out.println(e.getMessage());
				return false;
				
			}
		}
		}
		  
	return true;
	  
  }
}
