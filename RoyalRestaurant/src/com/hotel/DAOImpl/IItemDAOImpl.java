//@ nayaab
package com.hotel.DAOImpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.hotel.DAOService.DBConnect;
import com.hotel.DAOService.IItemDAO;
import com.hotel.bean.Item;

public class IItemDAOImpl implements IItemDAO {
	Connection connection = null;
	@Override
	public boolean addItemtoDB(Item item) {
		PreparedStatement statement=null;
		String insertQuery = "insert into items(item_name,item_price,item_cookingtime,item_category,item_availability) values(?,?,?,?,?)";

		boolean flag = false;
		try {
			connection = DBConnect.openConnection();
			System.out.println(connection);
			statement = connection.prepareStatement(insertQuery);

			statement.setString(1, item.getItemName());
			statement.setInt(2, item.getPrice());
			statement.setInt(3, item.getCookingTime());
			statement.setString(4, item.getCategory());
			statement.setString(5, item.getAvailability());

			statement.execute();
			flag=true;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (statement != null) {
					statement.close();
					DBConnect.closeConnection();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}

		return flag;

	}

	@Override
	public boolean updateItemtoDB(Item item) {
		PreparedStatement statement = null;
		connection = DBConnect.openConnection();
		Item item1 = null;
		try {
			statement = connection.prepareStatement(
					"update items set item_name=?, item_price=?,item_cookingtime=?, item_category=?, item_availability=? where item_no = ?");
			statement.setString(1, item.getItemName());
			statement.setString(4, item.getCategory());
			statement.setString(5, item.getAvailability());
			statement.setInt(2, item.getPrice());
			statement.setInt(3, item.getCookingTime());
			statement.setInt(6, item.getItemNo());

			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				String itemname = rs.getString(1);
				String category = rs.getString(4);
				String availability = rs.getString(5);
				int price = rs.getInt(2);
				int cookingtime = rs.getInt(3);

				item1 = new Item();
				item1.setItemName(itemname);
				item1.setCategory(category);
				item1.setAvailability(availability);
				item1.setCookingTime(cookingtime);
				item1.setPrice(price);

			}
		} catch (SQLException e){ 
			System.out.println(e.getMessage());
		} finally {
			try {
				if (statement != null) {
					statement.close();
					DBConnect.closeConnection();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return false;

	}

	@Override
	public boolean deleteItemfromDB(int itemNo) {
		PreparedStatement statement = null;
		try {
			connection = DBConnect.openConnection();
			statement = connection.prepareStatement("update items set item_availability =? where item_no=? ");
			statement.setString(1, "N");
			statement.setInt(2, itemNo);
			statement.executeQuery();
			return true;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (statement != null) {
					statement.close();
					DBConnect.closeConnection();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return false;

	}

	@Override
	public List<Item> getAllItemsfromDB() {
		PreparedStatement statement = null;
		List<Item> itemlist = new ArrayList<Item>();

		try {
			connection = DBConnect.openConnection();

			System.out.println("check1111");
			statement = connection.prepareStatement("select * from items order by item_no ");

			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				Item item = new Item();
				String itemname = rs.getString(2);
				String category = rs.getString(5);
				String availability = rs.getString(6);
				int itemno = rs.getInt(1);
				int price = rs.getInt(3);
				int cookingtime = rs.getInt(4);

				item.setItemName(itemname);
				item.setItemNo(itemno);
				item.setCategory(category);
				item.setAvailability(availability);
				item.setCookingTime(cookingtime);
				item.setPrice(price);

				itemlist.add(item);

			}
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		finally {
			try {
				if (statement != null) {
					statement.close();
					DBConnect.closeConnection();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}

		return itemlist;
	}

	@Override
	public List<Item> getItemsByCategory(String category) {
		PreparedStatement statement = null;
		List<Item> itemlist = new ArrayList<>();
		Connection connection = DBConnect.openConnection();

		try {
			connection = DBConnect.openConnection();
			statement = connection.prepareStatement("select * from items where item_category = ? ");
			statement.setString(1, category);
			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				String itemname = rs.getString(2);
				String availability = rs.getString(6);
				int itemno = rs.getInt(1);
				int price = rs.getInt(3);
				int cookingtime = rs.getInt(4);

				Item item1 = new Item();
				item1.setItemName(itemname);
				item1.setItemNo(itemno);
				item1.setCategory(category);
				item1.setAvailability(availability);
				item1.setCookingTime(cookingtime);
				item1.setPrice(price);

				itemlist.add(item1);
				System.out.println(item1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (statement != null) {
					statement.close();
					DBConnect.closeConnection();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return itemlist;

	}

	@Override
	public Item getitemsByItemNofromDB(int itemNo) {
		PreparedStatement statement = null;
		Connection connection = DBConnect.openConnection();

		Item item1 = null;

		try {

			statement = connection.prepareStatement("select * from items where item_no = ? ");
			statement.setInt(1, itemNo);
			ResultSet rs = statement.executeQuery();

			while (rs.next()) {
				String itemname = rs.getString(2);
				String category = rs.getString(5);
				String availability = rs.getString(6);
				int price = rs.getInt(3);
				int cookingtime = rs.getInt(4);
				item1 = new Item();

				item1.setItemName(itemname);
				item1.setItemNo(itemNo);
				item1.setAvailability(availability);
				item1.setCookingTime(cookingtime);
				item1.setPrice(price);
				item1.setCategory(category);

			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (statement != null) {
					statement.close();
					DBConnect.closeConnection();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
		return item1;

	}

}
