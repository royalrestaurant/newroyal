package com.hotel.bean;

import java.util.*;

import com.hotel.DAOImpl.IOrderDAOImpl;

public class Order {
	private ArrayList<OrderedItem> orderedItems = new ArrayList<OrderedItem>();
	private int totalbill;
	private String paymentMode;
    private int orderno;
	public int getOrderno() {
		return orderno;
	}

	public void setOrderno(int orderno) {
		this.orderno = orderno;
	}

	public ArrayList<OrderedItem> getOrderedItems() {
		return orderedItems;
	}

	public void setOrderedItems(ArrayList<OrderedItem> orderedItems) {
		this.orderedItems = orderedItems;
	}

	public int getTotalbill() {
		return totalbill;
	}

	public void setTotalbill(int totalbill) {
		this.totalbill = totalbill;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public void totalbill() {
		for (OrderedItem item : orderedItems) {
			totalbill += item.getQuantity() * item.getPrice();
			System.out.println(totalbill);
		}
       
	}

	public int totalCookingTime() {
		int cookingtime=0;
		for (OrderedItem item : orderedItems) {
			cookingtime += item.getQuantity() * item.getCookingTime();
		}
		return cookingtime;
	}
	public void placeorder(){
		IOrderDAOImpl iorder=new IOrderDAOImpl();
		iorder.getSequenceNum(this);
		iorder.addOrdertoDB(this);
		/*try {
			
			Thread.sleep(totalCookingTime()*1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	}

	@Override
	public String toString() {
		return "Order [orderedItems=" + orderedItems + ", totalbill=" + totalbill + ", paymentMode=" + paymentMode
				+ "]";
	}
}
