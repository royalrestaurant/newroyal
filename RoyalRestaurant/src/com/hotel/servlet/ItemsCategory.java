package com.hotel.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hotel.bean.Item;
import com.hotel.service.IOrder;
import com.hotel.serviceImpl.IOrderImpl;

/**
 * Servlet implementation class ItemsCategory
 */
@WebServlet("/itemsCategory")
public class ItemsCategory extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ItemsCategory() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		String category=request.getParameter("category");
		IOrder iorder=new IOrderImpl();
		List<Item> itemList=iorder.getItemsByCategory(category);
		request.setAttribute("ItemList", itemList);
		RequestDispatcher rd=request.getRequestDispatcher("displayItems.jsp");
		rd.forward(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}
