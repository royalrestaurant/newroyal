package com.hotel.servlet;

import java.io.IOException;

import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hotel.bean.Item;
import com.hotel.service.IItem;
import com.hotel.serviceImpl.IItemImpl;

/**
 * @author naveen called from admin.jsp on clicking view all items. then it is
 *         followed to viewallitems.jsp Servlet implementation class
 *         ViewAllItemsServlet
 */

@WebServlet("/viewallitemsservlet")

public class ViewAllItemsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ViewAllItemsServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("raju");
		List<Item> allitems = null;
		IItem temp = new IItemImpl();
		allitems = temp.getAllItems();
		System.out.println("check");
		System.out.println();
		System.out.println(allitems);

		request.setAttribute("allitems", allitems);

		System.out.println("hi");

		RequestDispatcher rd = request.getRequestDispatcher("viewallitems.jsp");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
