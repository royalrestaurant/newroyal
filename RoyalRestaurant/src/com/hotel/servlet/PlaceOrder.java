package com.hotel.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.hotel.DAOImpl.IOrderDAOImpl;
import com.hotel.DAOService.IOrderDAO;
import com.hotel.bean.Order;
import com.hotel.bean.OrderedItem;
import com.hotel.service.IOrder;

/**
 * Servlet implementation class PlaceOrder
 */
@WebServlet("/placeOrder")
public class PlaceOrder extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PlaceOrder() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		PrintWriter out=response.getWriter();
		HttpSession session=request.getSession();
		Order order=(Order) session.getAttribute("ORDER");
		order.setPaymentMode(request.getParameter("paymentmode"));
		ArrayList<OrderedItem> itemlist=order.getOrderedItems();
		System.out.println(itemlist);
		order.placeorder();
		RequestDispatcher rd=request.getRequestDispatcher("happyMeal.jsp");
		rd.forward(request, response);
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
