package com.hotel.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.catalina.Session;

import com.hotel.DAOImpl.IItemDAOImpl;
import com.hotel.DAOService.IItemDAO;
import com.hotel.bean.Item;


/**@author rohan
 * Servlet implementation class EditServletDisplay
 */
@WebServlet("/EditServletDisplay")
public class EditServletDisplay extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditServletDisplay() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		PrintWriter out=response.getWriter();
        String itemname = request.getParameter("itemname");
        int itemno = Integer.parseInt(request.getParameter("itemno"));
        String rate=request.getParameter("price");
        String time=request.getParameter("cookingtime");
        /*RequestDispatcher rd=request.getRequestDispatcher("EditServlet?itemNo="+itemno);
        if(rate==""||time==""){
        	out.println("<h3> can not be empty values</h3>");
			rd.include(request,response);
        }else{*/
        	/*try{*/
        int price = Integer.parseInt(rate);
        String category = request.getParameter("category");
       
        int cookingtime = Integer.parseInt(time);
        /*if(price<=0||cookingtime<=0){
            out.println("<h3> can not be negative values</h3>");
			rd.forward(request,response);
        }else{*/
        String availability = request.getParameter("availability");
        RequestDispatcher rd1=request.getRequestDispatcher("viewallitemsservlet");
        Item item = new Item();
        item.setItemName(itemname);
        item.setItemNo(itemno);
        item.setPrice(price);
        item.setCategory(category);
        item.setCookingTime(cookingtime);
        item.setAvailability(availability);
        IItemDAO ref = new IItemDAOImpl();
        ref.updateItemtoDB(item);
        List<Item> itemlist = new ArrayList<>();
        itemlist.add(item);
        System.out.println(itemlist);
        
       
        rd1.include(request, response);
        	/*}*/
        	/*}catch(Exception e){
        		out.println("<h3> only integer values</h3>");
            	
    			rd.include(request,response);
        	}
        }*/
        
        
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
