package com.hotel.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import com.hotel.service.IItem;
import com.hotel.serviceImpl.IItemImpl;

/**@author naveen
 * deleting item by item no got from admin in viewallitems.jsp
 * Servlet implementation class DeleteItemServlet
 */
@WebServlet("/deleteItemServlet")
public class DeleteItemServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteItemServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println(request.getParameter("itemNo"));
		int itemNo= Integer.parseInt(request.getParameter("itemNo"));
		System.out.println();
		System.out.println(itemNo);
		IItem temp= new IItemImpl();
		boolean flag=temp.deleteItem(itemNo);
		if(flag){
		response.sendRedirect("viewallitemsservlet");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
