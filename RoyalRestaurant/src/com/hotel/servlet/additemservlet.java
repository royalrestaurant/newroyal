// by Parantap
package com.hotel.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hotel.bean.Item;
import com.hotel.service.IItem;
import com.hotel.serviceImpl.IItemImpl;

/**
 * Servlet implementation class additemservlet
 */
@WebServlet("/additemservlet")
public class additemservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public additemservlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		
		String itemName=request.getParameter("itemName");
		String category=request.getParameter("category");
		String availability=request.getParameter("availability");
		String rate=request.getParameter("price");
		String time=request.getParameter("cookingTime");
		System.out.println(category);
		RequestDispatcher rd=request.getRequestDispatcher("additemform.jsp");
		if(itemName==""||category==""||availability==""||rate==""||time==""){
			out.println("<h3> can not be null</h3>");
			rd.include(request,response);
		}else{
		try{
		int price=Integer.parseInt(rate);
		int cookingTime=Integer.parseInt(time);
		if(price<=0||cookingTime<=0){
			out.println("<h3>Price or Cooking time cannot be negative</h3>");
			rd.include(request,response);
		}
		 
		else{
		Item item=new Item();
		item.setItemName(itemName);
		item.setCategory(category);
		item.setAvailability(availability);
		item.setPrice(price);
		item.setCookingTime(cookingTime);
		
		IItem iitem=new IItemImpl();
		
		
		
		boolean status=iitem.addItem(item);  
	        if(status){  
	            out.print("<p>Item added successfully!</p>");  
	           // request.getRequestDispatcher("additemform.jsp").include(request, response); 
	            rd.forward(request, response);
	        }else{  
	            out.println("Sorry! unable to add item");  
	        }  
		out.close();
		}
		}catch(Exception e){
			out.println("<h3>only numbers should be entered</h3>");
			
			rd.include(request,response);
		}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
