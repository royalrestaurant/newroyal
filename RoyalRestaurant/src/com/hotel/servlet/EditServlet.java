package com.hotel.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hotel.DAOImpl.IItemDAOImpl;
import com.hotel.DAOService.IItemDAO;
import com.hotel.bean.Item;

/**@author rohan
 * Servlet implementation class EditServlet
 */
@WebServlet("/EditServlet")
public class EditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public EditServlet() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		  
        System.out.println("Inside Edit");
        IItemDAO ref = new IItemDAOImpl();
        
        int itemNo=Integer.parseInt(request.getParameter("itemNo"));
		Item item = ref.getitemsByItemNofromDB(itemNo);
		request.setAttribute("id", item);
		
		
		
		RequestDispatcher rd = request.getRequestDispatcher("edit.jsp");
		rd.include(request, response);
		
        
        
        
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
