package com.hotel.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.hotel.bean.Item;
import com.hotel.bean.OrderedItem;

/**
 * Servlet implementation class GetOrderItems
 */
//@WebServlet("/getOrderItems")
public class GetOrderItems extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetOrderItems() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
   response.setContentType("text/html");
	PrintWriter out=response.getWriter();
	String[] arr=request.getParameterValues("items");
	HttpSession session=request.getSession(false);
	ArrayList<OrderedItem> itemList=null; 
	  if(session==null){
		 session=request.getSession(); 
	  }
	 if(session.getAttribute("ItemList")==null){
		 itemList=new ArrayList<OrderedItem>();
		 session.setAttribute("ItemList", itemList);
	 }else{
		 itemList=(ArrayList<OrderedItem>)session.getAttribute("ItemList");
	 }
	 if(arr==null){
		 out.println("<b>you did not select any item</b><br>");
	 }else{
	  for(int i=0;i<arr.length;i++){
		  OrderedItem item=new OrderedItem();
		  String s[]=arr[i].split(",");
		  boolean flag=false;
		  int itemNo=Integer.parseInt(s[0]);
		  item.setItemNo(itemNo);
		  item.setItemName(s[1]);
		  item.setCategory(s[2]);
		  item.setPrice(Integer.parseInt(s[3]));
		  item.setCookingTime(Integer.parseInt(s[4]));
		  item.setQuantity(Integer.parseInt(request.getParameter(s[0])));
		  for(int j=0;j<itemList.size();j++){
			  if(itemList.get(j).getItemNo()==itemNo){
				  itemList.get(j).setQuantity(itemList.get(j).getQuantity()+item.getQuantity());
				  flag=true;
				  break;
			  }
		  }
		  if(!flag){
		  itemList.add(item);
		  }
	   }
	  out.println("<b>Item added to cart sucessfully</b><br>");
	 } 
	  RequestDispatcher rd=request.getRequestDispatcher("customer.jsp");
	  rd.include(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
