package com.hotel.DAOService;

import com.hotel.bean.Item;
import java.util.*;
public interface IItemDAO {
	public boolean addItemtoDB(Item item);
	public boolean updateItemtoDB(Item item);
	public boolean deleteItemfromDB(int itemNo);
	public List<Item> getAllItemsfromDB();
	public List<Item> getItemsByCategory(String category);
    public Item getitemsByItemNofromDB(int itemNo);
	
}