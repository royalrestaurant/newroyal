package com.hotel.DAOService;
import java.util.List;
import com.hotel.bean.Item;
import com.hotel.bean.Order;
public interface IOrderDAO {
public List<Item> getItemsByCategoryfromDB(String category);
public boolean addOrdertoDB(Order order);
}
