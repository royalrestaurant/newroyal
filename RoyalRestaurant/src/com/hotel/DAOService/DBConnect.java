package com.hotel.DAOService;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class DBConnect {
	static Connection connection; 
	public static Connection openConnection() {
		String driverName = "oracle.jdbc.driver.OracleDriver";
		String url = "jdbc:oracle:thin:@10.150.222.50:1521:xe";
		String username = "prudhviraju";
		String password = "prudhviraju";
		
		try {
			Class.forName(driverName);
			connection =  DriverManager.getConnection(url, username, password);
		} catch (Exception e) {
			System.out.println(e);
			System.out.println("error");
		}
		return connection;
	}

	public static void closeConnection() {
		try{
		if (connection != null){
			connection.close();
		}
		}catch(SQLException e) {
            e.printStackTrace();
         }
	}
}

