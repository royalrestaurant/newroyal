package com.hotel.serviceImpl;


import java.util.List;

import com.hotel.DAOImpl.IItemDAOImpl;
import com.hotel.DAOService.IItemDAO;
import com.hotel.bean.Item;
import com.hotel.service.IItem;

//@author rohan


public class IItemImpl implements IItem {
IItemDAO ref = new IItemDAOImpl(); 
	@Override
	public boolean addItem(Item item) {
		// TODO Auto-generated method stub
		return ref.addItemtoDB(item);
	}

	@Override
	public boolean updateItem(Item item) {
		// TODO Auto-generated method stub
		return ref.updateItemtoDB(item);
	}

	@Override
	public boolean deleteItem(int itemNo) {
		// TODO Auto-generated method stub
		return ref.deleteItemfromDB(itemNo);
	}

	@Override
	public List<Item> getAllItems() {
		// TODO Auto-generated method stub
		return ref.getAllItemsfromDB();
	}

	@Override
	public List<Item> getItemsByCategory(String category) {
		// TODO Auto-generated method stub
		return ref.getItemsByCategory(category);
	}

	@Override
	public Item getitemsByItemNo(int itemNo) {
		// TODO Auto-generated method stub
		return ref.getitemsByItemNofromDB(itemNo);
	}
	
}
